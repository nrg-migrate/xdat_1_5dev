//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/* 
 * XDAT � Extensible Data Archive Toolkit
 * Copyright (C) 2005 Washington University
 */
/*
 * Created on Jan 13, 2005
 *
 */
package org.nrg.xdat.exceptions;

/**
 * @author Tim
 *
 */
@SuppressWarnings("serial")
public class IllegalAccessException extends Exception {
	public IllegalAccessException(String message)
	{
		super(message);
	}
}

