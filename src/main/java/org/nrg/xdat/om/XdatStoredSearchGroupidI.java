// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Jul 18 12:37:49 CDT 2007
 *
 */
package org.nrg.xdat.om;

/**
 * @author XDAT
 *
 */
public interface XdatStoredSearchGroupidI {

	public String getSchemaElementName();

	/**
	 * @return Returns the groupID.
	 */
	public String getGroupid();

	/**
	 * Sets the value for groupID.
	 * @param v Value to Set.
	 */
	public void setGroupid(String v);

	/**
	 * @return Returns the xdat_stored_search_groupID_id.
	 */
	public Integer getXdatStoredSearchGroupidId();

	/**
	 * Sets the value for xdat_stored_search_groupID_id.
	 * @param v Value to Set.
	 */
	public void setXdatStoredSearchGroupidId(Integer v);
}
