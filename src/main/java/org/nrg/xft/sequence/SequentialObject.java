//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Created on May 5, 2005
 *
 */
package org.nrg.xft.sequence;


/**
 * @author Tim
 *
 */
public interface SequentialObject {
    public int getSequence();
    public void setSequence(int sequence);
}
