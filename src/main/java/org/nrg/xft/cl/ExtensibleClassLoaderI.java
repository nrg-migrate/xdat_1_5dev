// Copyright 2010 Washington University School of Medicine All Rights Reserved
package org.nrg.xft.cl;

public interface ExtensibleClassLoaderI {
	public Class getClass(String identifier);
}
